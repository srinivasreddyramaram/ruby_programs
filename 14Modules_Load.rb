puts "#$LOAD_PATH"
$LOAD_PATH.push('.') #dot represents current working directory

require "13Modules"
class Vehicle
	NAME = "Both types of vehicles"
	
	include Bike
	include Car

	def self.info
		puts "All Vehicles Info"
	end
	def ginsu()
		puts "General Insurance"
	end
end


Vehicle.info  #info was self method so object not needed, can be called as it is
puts Vehicle::NAME
v= Vehicle.new
#v.info

puts Car::NAME
Car.info
Bike.info