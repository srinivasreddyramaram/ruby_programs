require 'test/unit'
class SimpleNumber
	def initialize(num)
		raise unless num.kind_of? Numeric
		@x=num
	end

	def add(y)
		@x*y
	end
end

class TestSimpleNumer < Test::Unit::TestCase
	def test_simple
		assert_equal(5, SimpleNumber.new(2).add(2) )
	end
end