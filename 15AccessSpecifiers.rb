#access specifiers - private, public, protected

class Car
	attr_accessor :name, :model, :color
	@@chasis_no = 100 #class variable
	
	#constructor calling before any method automatically
	def initialize(name, model, color) #local variables
		@@chasis_no+=1
		@name=name #instance variables
		@model=model #instance variables
		@color=color #instance variables
		puts "New car with chassis no. #{@@chasis_no}"
	end

	
	def info()
		puts "Car name: #{@name}"
		puts "Car Model is #{@model}"
		puts "Car color is #{@color}"
		display
	end

	
	def display
		puts "This is Car display method"
	end

	protected :display
end
