#Swapping of three data types
x=1
y="cnu"
z=2.5

=begin
puts "before swap: #{x}, #{y}, #{z}"
=end

x,y,z=z,y,x

puts "after swap: #{x}, #{y}, #{z}"