#OOP in Ruby
#Simple class with methods
class Car
	@@chasis_no = 100 #class variable
	
	#constructor calling before any method automatically
	def initialize(name, model, color) #local variables
		@@chasis_no+=1
		@name=name #instance variables
		@mode=model #instance variables
		@color=color #instance variables
		puts "New car with chassis no. #{@@chasis_no}"
	end

	def info()
		puts "Car name: #{@name}"
		puts "Car Model is #{@model}"
		puts "Car color is #{@color}"
	end

	#get and set methods
	def getname
		@name
	end
	def setname=(value)
		@name=value
	end
	def getmodels
		@model
	end
	def setMode=(value)
		@model=value
	end

end

class DeluxCar < Car
	def initialize(name, model, color, gear)
		super(name, model, color)
		@gear=gear
		puts "Gear type is initialized"
	end

	def speed()
		puts "high speed car"
	end

	def info()
		super()
		puts "Delux gear type is #{@gear}"
	end
end


=begin
kwid = Car.new("Kwid1", "Diesel", "Yellow")
kwid.info
kwid2 = Car.new("Kwid2", "Diesel", "Blue")
kwid2.info
kwid3 = Car.new("Kwid3", "Petrol", "White")
kwid3.info
#print kwid.methods
=end

#using set to change attributes
kwid = DeluxCar.new("Kwid1", "Diesel", "Yellow", "Auto")
kwid.info
kwid.setname="Kwid5"
kwid.setMode="Nano"
#kwid.info
