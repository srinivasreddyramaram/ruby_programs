#OOP in Ruby
#Simple class with methods
class Car
	attr_accessor :name, :color, :model #gives the set and get permissions
	@@chasis_no = 100 #class variable
	
	#constructor calling before any method automatically
	def initialize(name, model, color) #local variables
		@@chasis_no+=1
		@name=name #instance variables
		@model=model #instance variables
		@color=color #instance variables
		puts "New car with chassis no. #{@@chasis_no}"
	end

	def info()
		puts "Car name: #{@name}"
		puts "Car Model is #{@model}"
		puts "Car color is #{@color}"
	end

end

#using set to change attributes
kwid = Car.new("Kwid1", "Petrol", "Yellow")
kwid.info
kwid.name="Kwid5"
kwid.color="blue"
kwid.model="Pet"
kwid.info
