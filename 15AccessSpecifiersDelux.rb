$LOAD_PATH.push(".")
require '15AccessSpecifiers'
class DeluxCar < Car
	def initialize(name, model, color, gear)
		super(name, model, color)
		@gear=gear
		puts "Gear type is initialized"
	end

	def speed()
		puts "high speed car"
	end

	def info()
		super()
		puts "Delux gear type is #{@gear}"
		display
	end
end

kwid = DeluxCar.new("Kwid1", "Diesel", "Yellow", "Auto")
kwid.info
#kwid.display
