#Exception handling

begin 
	raise 'a test exception'
rescue Exception =>e
	puts e.message
	puts e.backtrace.inspect
ensure 
	puts "Ensuring execution"
end


#another way
begin 
	#raise 'a test exception'
	puts "I am not raising an exception"
	#res = 1/0
rescue Exception =>e
	puts e.message
	puts e.backtrace.inspect
else
	puts "Congrats - no exceptions"
ensure 
	puts "Ensuring execution"
end
