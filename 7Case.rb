#Bank Menu using a case control structure
=begin
Customer can see bank name, account no, balance, option to credit/debit
Display features of the bank app
=end
i=1
while (i!=5)
	print "\n\nWelcome to Ruby Bank! We offer the following services...\n
		   Press 1 to check your current balance\n
		   Press 2 for deposit of amount\n
		   Press 3 for withdrawal of amount\n
		   Press 4 for a mini statement\n
		   Press 5 to exit\n
		   Choose an option...\n\n"
 	i=gets.chomp.to_i

 	msg = case i
		when 1 then "Your balance is xx"\
		when 2 then "How much would like to deposit?"
		when 3 then "How much do you want to withdraw?"
		when 4 then "This is your mini statment...."
		when 5 then "Thank you....Please visit again"
		else "Invalid input....Try again"
 	end

 	puts msg
end