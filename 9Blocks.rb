#Blocks and yield, Proc and Lambda

#yield with no params
def test
	puts "You are in test method"
	yield
	puts "You are back in test again!"
	yield
end
#test{puts "Welcome to test block"}


#yield with parameters
def test2
	puts "You are in test2"
	yield "cnu", "babu"
	puts "You are back!"
	yield "sono"
end
#test2{|i,j| puts "Hey #{i} and #{j}, you are in block"}

#simple block as iterator
#[1,2,3].each { |x| puts x*2}


#Proc and Lambda
p = Proc.new {|x| puts x*2}
#[1,2,3].each(&p)

proc = Proc.new {puts "Hello all, this is proc in a block!"}
#proc.call

lam = lambda {|x| puts x*2}
#[1,2,3].each(&lam)

lam = lambda { puts "Hello all, this is lambda in a block"}
#lam.call

lam = lambda {|x| puts x}
#lam.call(2)
#lam.call(2,3) #ArgumentError due to wrong number of args

proc = Proc.new{|x| puts x}
proc.call(2)
proc.call(2,3) #No args error is thrown

#Both lambda and proc work similar, lambda calls proc internally
puts lam.class
puts proc.class
