#Examples on methods and exercises

#method to say hello
def say_hello
	puts "Hello Class"
end
#say_hello

#method to add two numbers - note that puts or return is not necessary
def add(x,y)
	x+y
end
#puts add(2,3)

# to factorial of any number
def find_factorial(input_num)
factorial =1
num = input_num
while num!=0
	factorial = factorial * num
	num = num-1
end
puts "Factorial of #{input_num} is #{factorial}"
end

#another way to find factorial
def fact(n)
	if n<=0
		return 1
	else 
		return n*fact(n-1)
	end
end
#puts "Whose factorial do you want?"
#input_num = gets.chomp.to_i
#puts fact(input_num)


#to find fibnocci series upto given number
def fib(n)
	first_fib, second_fib=0,1
	puts first_fib, second_fib
	
	for i in 1..n-2
		next_fib = first_fib+second_fib
		first_fib, second_fib = second_fib, next_fib
		puts next_fib
	end
end
#fib(10)


#to accept any number of parameters - special symbol *
def varg(*arg)
	puts "you have given #{arg}"
end
#varg(4, 5, "hey")

#create a multiplier method to multiply all the given numbers in variable args
def multarg(*nums)
	puts nums
	prod = 1
	for i in nums
		prod = prod*i
	end
	puts "Product of #{nums} is #{prod}"
end
#multarg(1, 2, 3, 4, 5)
