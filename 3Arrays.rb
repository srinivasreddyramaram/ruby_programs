#array as a stack
books=[]
books.push('Rich Dad Poor Dad')
books.push('Huckleberry Finn')
books.push('The Intelligent Investor')
print "Books i had are #{books}\n"
books.pop
print "Books I now have are #{books}\n"

#array as a queue
books.unshift('Spring in Action')
books.unshift('Earthlings')

puts "Books after unshift are #{books}"

books.shift
puts "Books after shift are #{books}"

#linked list implementation
books.insert(2,'Programming in C')

puts "Books after insert are #{books}"

books.delete("Huckleberry Finn")

puts "Books I have actually read are #{books}"

puts "Books in reverse are #{books.reverse}"

puts "Books are sorted as #{books.sort!}"

#array to strings
strBooks = books.join(",")
puts strBooks

#strings to array
newBooks = strBooks.split(",")
print newBooks
