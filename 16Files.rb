#File handling

f=open('.\dataFiles\tdata.txt','w+')
f.write("This is a test data in the file\n")
f.close

f=open('.\dataFiles\tdata.txt')
puts f.read()
f.close

f=open('.\dataFiles\tdata.txt', 'a+')
f.write("This is second line - appended\n")
f.close

f=open('.\dataFiles\tdata.txt')
puts f.read
f.close

#another way - File closes automatically
File.open('.\dataFiles\sdata.txt', 'w') do |file| 
	file.puts "I am the Dragon Warrior"
end
File.open('.\dataFiles\sdata.txt', 'r') { |file| puts file.read}
