class Customer
	@@id = 1000
	attr_accessor :name, :phone
	def initialize(name, phone)
		@@id+=1
		@name = name
		@phone = phone
		
	end

	def customer_info
		print "id: #{@@id} \tName: #{@name}\tPhone:#{@phone}\n"
	end

end

class Motor_Insurance < Customer
	attr_accessor :registrationNo, :premiumAmount, :sumAssured
	def initialize(name, phone, registrationNo, premiumAmount, sumAssured)
		super(name, phone)
		@registrationNo = registrationNo
		@premiumAmount = premiumAmount
		@sumAssured = sumAssured
	end
	def motor_info
		print "RegistrationNo: #{@registrationNo}\tpremiumAmount: #{@premiumAmount}\n"
	end

end

class Life_Insurance < Customer
	attr_accessor :tenure, :sumAssured, :premiumAmount
	def initialize(name, phone, tenure, sumAssured, premiumAmount)
		super(name, phone)
		@tenure = tenure
		@sumAssured = sumAssured
		@premiumAmount = premiumAmount
	end
	def life_info
		print "tenure: #{@sumAssured}\tsumAssured: #{@sumAssured}\tpremiumAmount:#{@premiumAmount}\n"
	end
end

=begin
motor1 = Motor_Insurance.new("MO1234", "Srinivas", "Medchal, Telangana", 2, "TS07BG3609", 1500.0, 100000.0, "30-12-2018")
motor1.customer_info
motor1.motor_info

life1= Life_Insurance.new("LI4567", "Moon", "Disukhnagar, Telangana", "01-01-2018", 48, 5000000.0, 3000.0, "31-12-2018")
life1.customer_info
life1.life_info
=end

welcome_msg="\n\n\ *********Welcome to LastMinute Insurance Corporation*********\n
			   Following are our services....\n
			   1. Buy a policy\n
			   2. View Customer Info\n\
			   3. Exit\n
			   Choose an option to start....\n\n"
offered_policies = "(1) Motor Insurance\t (2)Life Insurance"


while true
	puts welcome_msg
	main_menu_selection = gets.chomp.to_i
	case main_menu_selection
		when 1 
			print "As of now, we are offering: #{offered_policies}\n
				Choose one by pressing the number...\n\n"
			insurance_selection = gets.chomp.to_i
			if insurance_selection==1
				m1 = Motor_Insurance.new("Srinivas", "9573028751", "TS07BG3609", 1500.0, 100000.0)
				m1.customer_info
				m1.motor_info
			elsif insurance_selection==2
				l1= Life_Insurance.new("Moon", "9573028751", 48, 5000000.0, 3000.0)
				l1.customer_info
				l1.life_info
			end
		when 2
			print "\nYour details are:\n"
			c1 = Customer.new("Srinivas", "9573028751")
			c1.customer_info
		when 3
			print "\nThank you, visit again\n\n"
			exit
		else
			print "\nInvalid option. Please try again...."
		
	end
end
