#Hashes Case Study - Books, Publishers and Authors

pub1 ={
	:pname => "S Chand Co",
	:pcity => "Hyd",
	:pweb=> "schand.com"
}

pub2 ={
	:pname => "Hilling Books",
	:pcity => "Shimla",
	:pweb=> "shillong.com"
}

publishers = {:p1 => pub1, :p2 => pub2}
#puts "Publishers are #{publishers}"

auth1 ={
	:fname =>"Yashwanth",
	:lname => "Kanetkar",
	:city => "Jaipur"
}


auth2 ={
	:fname =>"Robin",
	:lname => "Sharma",
	:city => "London"
}

auth3 ={
	:fname =>"JK",
	:lname => "Rowling",
	:city => "Doolpet"
}

authors = {
	:a1 => auth1,
	:a2 => auth2,
	:a3 => auth3
}
#puts "Authors are #{authors}"

book1 = {
	:title => "Let us C",
	:publisher => pub1,
	:author => [authors[:a1], authors[:a2]],
	:dop => "20.01.2018"
}
book2 = {
	:title => "Spring in Action",
	:publisher => pub2,
	:author => [authors[:a2],authors[:a3]],
	:dop => "02.02.2018"
}

books = {
	:b1 => book1,
	:b2 => book2
}

#puts "Books are #{books}"

print "Books keys are #{books.keys}\n\n"

#print "Books values are #{books.values}\n\n"

print "Second Book Keys are #{books[:b2].keys} \n\n"

print "Second book authors are #{books[:b2][:author]}\n\n"

print "Second book first author is #{books[:b2][:author][0]}\n\n"

books[:b2][:author][0][:fname] = "Anushka"

print "Second book first author is #{books[:b2][:author][0]}\n\n"
